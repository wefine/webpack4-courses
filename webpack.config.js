const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const path = require('path');
const projectDir = __dirname;

module.exports = {
    // 模式有development、production 和 none 三种
    mode: 'development',
    entry: projectDir + '/src/index.js',
    output: {
        path: path.resolve(projectDir, 'dist'),
        filename: 'main.js'
    },
    // optimization: {
    //     minimizer: [
    //         new UglifyJsPlugin({
    //             cache: false,
    //             parallel: true,
    //             sourceMap: true // set to true if you want JS source maps
    //         }),
    //         new OptimizeCSSAssetsPlugin({})
    //     ]
    // },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ template: projectDir + '/index.html' }),
        new MiniCssExtractPlugin()
    ]
};