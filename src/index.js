import './css/style.css';

import Dog from './js/dog.js';
import * as lib from './js/lib.js';

console.log(lib.sum(1, 2));
console.log(lib.substract(3, 1));
console.log(lib.divide(6, 3));

const dog = new Dog();
dog.bark(); // 'bark!'